class Slides extends React.Component{
    render(){
        return(
            <div className="slides" style={{
                width: 100 * this.props.slides + "%",
                left: -(this.props.slide) * 100 + "%"
            }}>
                {this.props.children}
            </div>
        );
    }
}

class Slide extends React.Component{
    render(){
        return(
            <div className="slide" style={{
                backgroundColor: this.props.img
            }}>
                {this.props.children}
            </div>
        );
    }
}



class Slider extends React.Component {
    constructor(){
        super();
        this.state = {
            slide: 0
        };
        this.updataSlide();
    }

    updataSlide(){
        setInterval(()=>{
            if(this.props.slides -1 > this.state.slide){
                this.setState({
                    slide: this.state.slide + 1
                });
            }

            else {
                this.setState({slide: 0});
            }

            for(let i = 0; i < this.refs.dots.childNodes.length; i++) {
                this.refs.dots.childNodes[i].className = " dot";
            }

            this.refs.dots.childNodes[this.state.slide].className += " dot_active";
        },4000)
    }
    activeSlide(number) {
        this.setState({slide: number}, ()=>{
            this.refs.dots.childNodes[this.state.slide].className += " dot_active";
        });

        for(let i = 0; i < this.refs.dots.childNodes.length; i++) {
            this.refs.dots.childNodes[i].className = "dot";
        }

    }
    render() {
        return(
            <div className="slider">
                <div className="dots" ref={"dots"}>
                    <div className="dot dot_active" onClick={()=>{this.activeSlide(0)}}> </div>
                    <div className="dot" onClick={()=>{this.activeSlide(1)}}> </div>
                    <div className="dot" onClick={()=>{this.activeSlide(2)}}> </div>
                    <div className="dot" onClick={()=>{this.activeSlide(3)}}> </div>
                </div>
                <Slides slides={this.props.slides} slide={this.state.slide}>
                    <Slide img={"black"}>
                        <img src="./img/SLIDER%20IMAGE.png" alt="" className={"slider_image"}/>
                        <h6 className={"promotions_text_title"}>REACH FOR THE STARS <br/> WITH UP TO 200 SPINS</h6>
                        <p className={"promotions_text"}>Lorem ipsum dolor sit amet, consectetur adipisicing elit.At debitis eaque id iure labore temporibus voluptatibus.</p>
                    </Slide>
                    <Slide img={"green"}/>
                    <Slide img={"red"}/>
                    <Slide img={"gray"}/>
                </Slides>
            </div>
        );
    }

}


ReactDOM.render(<Slider slides={4}/>, document.getElementById("slider"));
