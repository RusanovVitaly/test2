let nav = document.getElementsByClassName("menu-trigger")[0];
let menu = document.getElementsByClassName("about-casino_holder")[0];

nav.onclick = ()=>{
    let current_left = menu.style.left;
    if (current_left === "" || (parseInt(current_left.slice(0, -2)) === -210)){
        menu.style.left = 0 + "px";
        nav.style.marginLeft = 220 + "px";
    }
    else{
        menu.style.left = -210 + "px";
        nav.style.marginLeft = 10 + "px";
    }
};